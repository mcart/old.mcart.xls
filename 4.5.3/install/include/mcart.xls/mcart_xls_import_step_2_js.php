<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 

global $MCART_IS_SKU;
$MCART_IS_SKU = false;
global $DB;
$db_type=strtolower($DB->type);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mcart.xls/classes/".$db_type."/profile.php"); 
if(
  (CModule::IncludeModule('catalog'))&&
  (CModule::IncludeModule('sale'))
  )
	$MCART_IS_SKU = true;
?>

<?
$inputFileName = $_SESSION["MCART_XLS_ARRAY"]['INPUT_FILENAME'];

$bEnd = false;
CModule::IncludeModule('mcart.xls');
 $langCls = new CMcartXlsStrRef();

 
 
$firstColumn = $_SESSION["MCART_XLS_ARRAY"]["DIAPAZONE_A"];
$highestColumn = $_SESSION["MCART_XLS_ARRAY"]["DIAPAZONE_Z"];	
$SECTION = $_SESSION["MCART_XLS_ARRAY"]["SECTION"];	
$SECTION_FOR_NEW = $_SESSION["MCART_XLS_ARRAY"]["SECTION_FOR_NEW"];	
$arrToInt = $_SESSION["MCART_XLS_ARRAY"]['ACTION_MODIFY']['TO_INT'];
$arrToLink = $_SESSION["MCART_XLS_ARRAY"]['ACTION_MODIFY']['TO_LINK'];
$arrTakeTwo = $_SESSION["MCART_XLS_ARRAY"]['ACTION_MODIFY']['TAKE_TWO'];
$arrTakeThree = $_SESSION["MCART_XLS_ARRAY"]['ACTION_MODIFY']['TAKE_THREE'];
$arrSubactionParams = $_SESSION["MCART_XLS_ARRAY"]['ACTION_MODIFY']["SUBACTION_PARAMS"];
$arrDelSubstr = $_SESSION["MCART_XLS_ARRAY"]['ACTION_MODIFY']["DEL_SUBSTR"];


$CATALOG_PRICE_BASE_ID = $_SESSION["MCART_XLS_ARRAY"]['CATALOG_PRICE_BASE_ID'];
$arrDatetime = array();
	
include $_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/mcart.xls/classes/general/PHPExcel/IOFactory.php';
	 
	 try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			
			if ($inputFileType=='CSV')
			{
				
				if (ini_get('mbstring.func_overload') & 2) 
				{
				
					die(GetMessage("MCART_WRONG_FILE_FORMAT")."</br><a href = '/bitrix/admin/mcart_xls_import.php'>".GetMessage("STEP_BACK")."</a>");
				
				}
			}
			
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
			$worksheet_names = $objReader->listWorksheetNames($inputFileName);
//print_r($worksheet_names);
			
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		$sheet = $objPHPExcel->getSheet($_SESSION['ARR_XLS_DATA']["SHEET_ID"]); 
		
		$trueHighestRow = $_SESSION["MCART_XLS_ARRAY"]["HIGHEST_ROW"];
		$firstRow = $_SESSION['START_ROW'];
		$highestRow =$firstRow+50;
		if ($highestRow>=$trueHighestRow)
			{
			$highestRow=$trueHighestRow;
			$bEnd = true;
			}
		
		$IDENTIFY =  $_SESSION["MCART_XLS_ARRAY"]['IDENTIFY'];
		
		$mergedCellsRange = $sheet->getMergeCells();
		
		for ($row = $firstRow; $row <= $highestRow; $row++)
		{ 
			$rowData = $sheet->rangeToArray($firstColumn . $row . ':' . $highestColumn . $row,
											NULL,
											TRUE,
											FALSE,
											true
											);
											
											
										
											
											foreach ($rowData as $row_id=>$row_val)
											{
												$col_id_int = 0;
												foreach($row_val as $col_id=>$txt)
												{
												
												$isInRange = false;
												
												$cell = $sheet->getCell($col_id.$row_id);
												
												foreach($mergedCellsRange as $currMergedRange) {
												
												if($cell->isInRange($currMergedRange)) {
														$currMergedCellsArray = PHPExcel_Cell::splitRange($currMergedRange);
														
														$merge2CELL = $sheet->getCell($currMergedCellsArray[0][0]);
														$merge_index = $row_id - $merge2CELL->GetRow();
														
														//if (!$merge2CELL->isInRange($firstColumn . $firstRow . ':' . $highestColumn . $highestRow))
														//	$skip = true;
														
														if (in_array($col_id, $arrToLink))
															{
															$tmpArrLink =  $sheet->getHyperlink($currMergedCellsArray[0][0]);
															$tmpLink = each($tmpArrLink);
															$txt = $tmpLink['value'];
															}
														elseif (in_array($col_id, $arrToInt))
															$txt = IntVal($langCls->ConvertArrayCharset($merge2CELL->getValue(), BP_EI_DIRECTION_IMPORT));
															
														else
															$txt = $merge2CELL->getFormattedValue();
															$arrRows[$row_id]["merge_index"] = $merge_index;
															$isInRange = true;
														break;
													}
													
												}
												
												
												if (!$isInRange)// если ячейка не-объединенная
												{
													if (in_array($col_id, $arrToLink))
														{
														$tmpArrLink =  $sheet->getHyperlink($col_id.$row_id);
														$tmpLink = each($tmpArrLink);
														$txt = $tmpLink['value'];
														}
													elseif (in_array($col_id, $arrToInt))
														$txt = IntVal($langCls->ConvertArrayCharset($txt, BP_EI_DIRECTION_IMPORT));
														
													elseif(in_array($col_id, $arrDatetime))	
														$txt = $cell->getFormattedValue();
														
													else	
														$txt = $langCls->ConvertArrayCharset($txt, BP_EI_DIRECTION_IMPORT);
												}
												
												$arrRows[$row_id][$col_id_int] = $txt;	
												$col_id_int++;
												}
											
											
											}
										
										
											
											
		}
		//print "<pre>"; print_r($arrRows); print "</pre>";
		
		$arrRowsMulti = array();
		$i = 0;
		$tmprowid_old = false;
		foreach ($arrRows as $tmpRow)
			{
			if (!empty($tmpRow[$IDENTIFY]))
				{
				
				$tmprowid = $tmpRow[$IDENTIFY];
				
				if ($tmprowid!==$tmprowid_old)
					{
					
					$tmprowid_old = $tmprowid;
					$i++;
					}
				
				$tmpID = $tmpRow["merge_index"];
				if (empty($tmpID))
					$tmpID = 0;
				
				$arrRowsMulti[$i][$tmpID] = $tmpRow;
				
				}
			
			}
			
		//print "<pre>"; print_r($arrRowsMulti); print "</pre>";		
		//==============================================================================================================================================================================
		//==============================================================================================================================================================================
		if (CModule::IncludeModule('iblock'))
{
	$ielcount = $_SESSION["MCART_XLS_ARRAY"]['ADD_COUNT'];
	$ierrcount = $_SESSION["MCART_XLS_ARRAY"]['ERR_COUNT'];
	$ielUpdcount = $_SESSION["MCART_XLS_ARRAY"]['UPDATE_COUNT'];
	
	
	//=====================================================	
	$PROP_COLUMNS = $_SESSION["MCART_XLS_ARRAY"]['COLUMNS'];
	
	$NAME_ID =  $_SESSION["MCART_XLS_ARRAY"]['NAME_ID'];
	$XLS_IBLOCK_ID = $_SESSION["MCART_XLS_ARRAY"]['IBLOCK_ID'];
	
	$arrSelectedFields = array("NAME", "IBLOCK_SECTION_ID", "IBLOCK_ID", "ID", "CODE");
	foreach ($PROP_COLUMNS as $key_col=>$arr_col)
		{
			foreach ($arr_col as $one_prop)
				{
				switch ($one_prop) {
						case 'FLD_AMOUNT':
							$field = "";
						break;
						case 'FLD_DETAIL_TEXT':
							$field = "DETAIL_TEXT";
							break;
						case 'FLD_PREVIEW_TEXT':
							$field = "PREVIEW_TEXT";
							break;
						case 'FLD_DETAIL_PICTURE':
							$field = "DETAIL_PICTURE";
							break;
						case 'FLD_PREVIEW_PICTURE':
							$field = "PREVIEW_PICTURE";
							break;	
						
						case "FLD_CATALOG_BASE_PRICE":
							$field = "";
							break;
						case "FLD_PURCHASING_PRICE":
							$field = "";
							break;
						default:
							$field = "PROPERTY_".$one_prop;
							}	
					if (!empty($field))		
						{
						$arrSelectedFields[] = $field;
						if ($IDENTIFY == $key_col)
							$IDENTIFY_CODE = $field."_VALUE";
						}
					}
				
		}				
		if ($NAME_ID==$IDENTIFY)
			$IDENTIFY_CODE = "NAME";
		

		
		if (empty($IDENTIFY_CODE))
			{
			echo GetMessage("WRONG_IDENTIFY_CODE");
			?>
			<a href = "/bitrix/admin/mcart_xls_import_step_1.php"><?=GetMessage("STEP_BACK")?></a>
			<?
			die();
			}
		$IBLOCK_ELEMENTS = array();
		$arrSelectedFields[] = "CATALOG_GROUP_".$CATALOG_PRICE_BASE_ID;
		$list = CIBlockElement::GetList(array("ID"=>"ASC"), array("IBLOCK_ID"=>$XLS_IBLOCK_ID), false, false, $arrSelectedFields);
			while ($search_el = $list->GetNext())
			if (!empty($search_el[$IDENTIFY_CODE]))
				{
				$IBLOCK_ELEMENTS[] = array("IDENTIFY"=>$search_el[$IDENTIFY_CODE], "ELEMENT"=>$search_el);
				}
		//======================================================================
		//print "<pre>"; print_r($IBLOCK_ELEMENTS); print "</pre>";
	
	foreach ($arrRowsMulti as $arr2Row)
	{
	$one_row = $arr2Row[0];
	if (empty($one_row))
		continue;
	
	if (empty($one_row[$IDENTIFY]))
		continue;
		
		$el = new CIBlockElement;

		$PROP = array();
		$detail_text = "";
		$preview_text = "";
		$amount = 0;
		$base_price = 0;
		$purchasing_price = 0;
		$arFilter = array();
		
		foreach ($PROP_COLUMNS as $key_col=>$arr_col)
		{
		if (in_array($key_col, $arrTakeTwo))
			$val_text = $arr2Row[1][$key_col];
		elseif (in_array($key_col, $arrTakeThree))
			$val_text = $arr2Row[2][$key_col];	
		else
			$val_text = trim($one_row[$key_col]);
//$arrSubactionParams 		
//$arrDelSubstr = $_SE



		
			if (in_array($key_col, $arrDelSubstr))	
				$val_text = str_replace($arrSubactionParams[$key_col], "", $val_text);
			
			if (!empty($val_text))
			{
			foreach ($arr_col as $one_prop)
				{
					switch ($one_prop) {
						case 'FLD_AMOUNT':
							$amount = IntVal($val_text);
						break;
						case 'FLD_DETAIL_TEXT':
							$detail_text =$val_text;
							break;
						case 'FLD_PREVIEW_TEXT':
							$preview_text = $val_text;
							break;
						case 'FLD_DETAIL_PICTURE':
							$URL = $val_text;
							$detail_picture = CFile::MakeFileArray($URL);
							break;
						case 'FLD_PREVIEW_PICTURE':
							$preview_picture = CFile::MakeFileArray($val_text);
							break;	
						
						case "FLD_CATALOG_BASE_PRICE":
							$base_price = $val_text;
							break;
						case "FLD_PURCHASING_PRICE":
							$purchasing_price = $val_text;
							break;
						default:
							{$PROP[$one_prop] = $val_text;
							if ($IDENTIFY == $key_col)
								//$arFilter["PROPERTY_".$one_prop] = $val_text;
								$IDENTIFY_VALUE=$val_text;
							}	
					}
				
				}
			}
		}
		
		if ($NAME_ID==$IDENTIFY)
			$IDENTIFY_VALUE = $one_row[$NAME_ID];

		
		if (empty($IDENTIFY_VALUE))
			continue;
		
		$SEARCH_EL_ID="";
		$PRODUCT_ID = "";
		
		foreach ($IBLOCK_ELEMENTS as $test_element)
			{
			if ($test_element["IDENTIFY"]==$IDENTIFY_VALUE)
				$search_el = $test_element["ELEMENT"];
			}
		
		if (isset($search_el))
		{	
		//$search_el = $IBLOCK_ELEMENTS[$IDENTIFY_VALUE];
		//print "<pre>"; print_r($search_el); print "</pre>";
			$SEARCH_EL_ID = $search_el["ID"];
			$section2 = $search_el["IBLOCK_SECTION_ID"];
			
			$bNeedUpdate = false;
			
			
			
			if (trim($search_el["~NAME"])!==$one_row[$NAME_ID])
				{
				$bNeedUpdate = true;
				
				}
			elseif ((trim($search_el["DETAIL_TEXT"])!==$detail_text)&&(!empty($detail_text)))	
				{
				$bNeedUpdate = true;
				//echo "неравно детально текст<br>";
				}
			elseif ((trim($search_el["PREVIEW_TEXT"])!==$preview_text)&&(!empty($preview_text)))
				{
				$bNeedUpdate = true;
				//echo "неравно текст превью<br>";
				}
			else
				{
				foreach ($PROP as $key_prop=>$prop_val)
					if (trim($search_el["PROPERTY_".$key_prop."_VALUE"])!=$prop_val)
						{
						$bNeedUpdate = true;	
						//echo "неравно ".$key_prop." текст<br>";
						break;
						}
				}
		}
		else
			{
			$section2 = false;
			$search_el = null;
			}
		
		if ($SECTION==0)
			$tmpSection = false;
		elseif ($SECTION == -1)	
			$tmpSection= $section2;
		else
			$tmpSection= $SECTION;
		
		$arLoadProductArray = Array(
		  "MODIFIED_BY"    => $USER->GetID(), 
		 // "IBLOCK_SECTION_ID" => $tmpSection,        
		  "IBLOCK_ID"      => $XLS_IBLOCK_ID,
		  "PROPERTY_VALUES"=> $PROP,
		  "NAME"           => trim($one_row[$NAME_ID]),
		  "ACTIVE"         => "Y",            // активен
		  "PREVIEW_TEXT"   => $preview_text,
		  "DETAIL_TEXT"    => $detail_text,
		  "CATALOG_PRICE_".$CATALOG_PRICE_BASE_ID =>$base_price, 
		  "DETAIL_PICTURE" => $detail_picture,
		  "PREVIEW_PICTURE" => $preview_picture
		  
		  );
		
		if ($MAKE_TRANSLIT_CODE)
		{
		
			$params = Array(
							"max_len" => "75", // обрезает символьный код до 75 символов
							"change_case" => "L", // буквы преобразуются к нижнему регистру
							"replace_space" => "-", // меняем пробелы на нижнее подчеркивание
							"replace_other" => "-", // меняем левые символы на нижнее подчеркивание
							"delete_repeat_replace" => "true", // удаляем повторяющиеся нижние подчеркивания
							"use_google" => "false", // отключаем использование google
						 );
				
				$CODE = CUtil::Translit($one_row[$NAME_ID], "ru", $params);
			$arLoadProductArray["CODE"] = $CODE;
			if ((!empty($search_el))&&($search_el["CODE"]!=$CODE))
				$bNeedUpdate = true;
			
		}
		
		if (!empty($SEARCH_EL_ID))
			{
			if ($bNeedUpdate)
				{
					$arLoadProductArray["IBLOCK_SECTION_ID"] = $tmpSection;
					//unset($arLoadProductArray["IBLOCK_SECTION_ID"]);
					
					unset($arLoadProductArray["PROPERTY_VALUES"]);
					
					$res = $el->Update($SEARCH_EL_ID, $arLoadProductArray, false, false, true);
					
					CIBlockElement::SetPropertyValuesEx($SEARCH_EL_ID, false, $PROP);	
					
					  $ielUpdcount++;
				}
			$PRODUCT_ID = $SEARCH_EL_ID;	
			
			}
			
			
		else
		{
		$arLoadProductArray["IBLOCK_SECTION_ID"] = $SECTION_FOR_NEW;
		if( $PRODUCT_ID = $el->Add($arLoadProductArray, false, false, true))
			{
			//echo GetMessage("XLS_ELEMENT_ADDED").$PRODUCT_ID."</br>";
			  $ielcount++;
			}
			
		else
		  {
		  $strError = $strError ."</br> ".$el->LAST_ERROR."</br>";
		  $ierrcount++;
		  }	
			
		}	
		if (!empty($PRODUCT_ID))
		
		  {	$SKU_ID ="";
			 
			
			
		  
			if ((!empty($SKU_IBLOCK_ID))&&(empty($SEARCH_EL_ID)))
			{	
			
				$PROP[$CML2_LINK_CODE] = $PRODUCT_ID;
				$arLoadSKUArray = Array(
					  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
					  "IBLOCK_SECTION_ID" => $SECTION,          // элемент лежит в корне раздела
					  "IBLOCK_ID"      => $SKU_IBLOCK_ID,
					  "PROPERTY_VALUES"=> $PROP,
					  "NAME"           => $one_row[$NAME_ID],
					  "ACTIVE"         => "Y",            // активен
					  "PREVIEW_TEXT"   => $preview_text,
					  "DETAIL_TEXT"    => $detail_text,
					  "CATALOG_PRICE_".$CATALOG_PRICE_BASE_ID =>$base_price, 
					  "DETAIL_PICTURE" => $detail_picture,
					  "PREVIEW_PICTURE" => $preview_picture
					  
					  );
				if($SKU_ID = $el->Add($arLoadSKUArray, false, false, true))
					{
					
					
					}
					

			}
			else
				{
				
				if (intval($amount)>0)
					
						CCatalogProduct::Update($PRODUCT_ID, array("QUANTITY"=>$amount));
				}
			 
			if (!empty($SKU_ID))
				$PRICE_PROD_ID = $SKU_ID;
			else	
				$PRICE_PROD_ID = $PRODUCT_ID;
			

			if ($MCART_IS_SKU)
			{	
			 if ($base_price>0)// добавление базовой цены
			  {
			  $base_price = str_replace(",", ".", $base_price);
			  CCatalogProduct::Add(array("ID"=>$PRICE_PROD_ID));
				
				$arCatalogFields = Array(
					"PRODUCT_ID" => $PRICE_PROD_ID,
					"CATALOG_GROUP_ID" => $CATALOG_PRICE_BASE_ID,
					"PRICE" => $base_price,
					"CURRENCY" => "RUB",
					"QUANTITY_FROM" => false,
					"QUANTITY_TO" => false
					
				);

					//$obPrice = new CPrice();
					
					$res = CPrice::GetList(
							array(),
							array(
									"PRODUCT_ID" => $PRICE_PROD_ID,
									"CATALOG_GROUP_ID" => $CATALOG_PRICE_BASE_ID
								)
						);

					if ($arr = $res->Fetch())
					{
						CPrice::Update($arr["ID"], $arCatalogFields);
					}
					else
					{
						CPrice::Add($arCatalogFields);
					}
					
					
					/*
					if (!$obPrice->Add($arCatalogFields,false))
					{
					 $e = $APPLICATION->GetException();
					 $str = $e->GetString();
					 echo $str;
					}
					*/
		  
			  }
			  
			  if($purchasing_price>0)// добавление закупочной цены
			  {
			  $purchasing_price = str_replace(",", ".", $purchasing_price);
				$arPurchFields = array("PURCHASING_PRICE" => $purchasing_price);// зарезервированное количество
				CCatalogProduct::Update($PRICE_PROD_ID, $arPurchFields);
			  }
			  
			 if (intval($amount)>0)
						CCatalogProduct::Update($PRICE_PROD_ID, array("QUANTITY"=>$amount));
			}
			  
		  }
		else
		  {$strError = $strError ."</br> ".$el->LAST_ERROR."</br>";
		  $ierrcount++;
		  }
			
			
			
	}
	/*	
	echo '</br>'.GetMessage("XLS_MCART_READY")."</br>".
	GetMessage("XLS_ADDED_COUNT").$ielcount."</br>".
	GetMessage("XLS_UPDATED_COUNT").$ielUpdcount."</br>".
	GetMessage("XLS_WITH_ERROR_COUNT").$ierrcount."</br>";
	*/
}
		
		//==============================================================================================================================================================================
		//==============================================================================================================================================================================
		
		
		
		$_SESSION['START_ROW'] = $highestRow+1;
		
		$_SESSION["MCART_XLS_ARRAY"]['ADD_COUNT'] = $ielcount;
	$_SESSION["MCART_XLS_ARRAY"]['ERR_COUNT'] = $ierrcount;
	$_SESSION["MCART_XLS_ARRAY"]['UPDATE_COUNT'] = $ielUpdcount;
		
		if ($bEnd)
			echo "The End</br>Added: ".$ielcount."</br>Updated: ".$ielUpdcount."</br>Errors: ".$ierrcount;
		else
			echo "string number ".$firstRow." to ".$highestRow.$strError;
?>