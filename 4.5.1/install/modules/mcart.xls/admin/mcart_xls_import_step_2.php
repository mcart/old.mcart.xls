<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); 
IncludeModuleLangFile( __FILE__);
$APPLICATION->SetTitle(GetMessage("MCART_IMPORT_XLS_STEP_2"));
CJSCore::Init("jquery");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
global $MCART_IS_SKU;
$MCART_IS_SKU = false;
global $DB;
$db_type=strtolower($DB->type);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mcart.xls/classes/".$db_type."/profile.php"); 
if(
  (CModule::IncludeModule('catalog'))&&
  (CModule::IncludeModule('sale'))
  )
	$MCART_IS_SKU = true;
	
	$firstColumn = $_REQUEST['firstColumn'];
	$firstRow = $_REQUEST['firstRow'];
	$titleRow = $_REQUEST['titleRow'];
	$highestColumn = $_REQUEST['highestColumn'];

	if (!$CML2_LINK_CODE = $_REQUEST['cml2_link_code'])
		$CML2_LINK_CODE = "";

		
		
	if ($_REQUEST["make_translit_code"]=="Y")
		$MAKE_TRANSLIT_CODE = true;
	else
		$MAKE_TRANSLIT_CODE =false;
		
	if ($_REQUEST['save_profile']=="Y")	
		{
		$IS_SAVE_PROFILE = true;
		$PROFILE_NAME = mysql_real_escape_string($_REQUEST['profile_name']);
		}
	else
		$IS_SAVE_PROFILE = false;
	
	
	
	if (!(isset($_REQUEST['fld_identify'])))
	{
ShowError(GetMessage("WRONG_IDENTIFY_CODE"));
die();
	}
$i = 0;
foreach(range(ord($firstColumn), ord($highestColumn)) as $v)
	{
	$alfavit[chr($v)] = $i;
	$i++;
	}	
	
$arrModify = $_REQUEST['modify_type'];
$arrModifySubaction = $_REQUEST['modify_subaction'];

$arrActionParams = $_REQUEST['modify_subaction_params'];

$arrToInt = array();
$arrToLink = array();
$arrTakeTwo = array();
$arrTakeThree = array();
$arrDelSubstr = array();
foreach ($arrModify as $litera=>$action)
	{
	if ($action=="XLS_MODIFY_TYPE_TO_LINK")
		$arrToLink[] = $litera;
	elseif ($action=="XLS_MODIFY_TYPE_TO_INT")	
		$arrToInt[] = $litera;
	elseif ($action=="XLS_TAKE_VALUE_TWO")	
		$arrTakeTwo[] = $alfavit[$litera];	
	elseif ($action=="XLS_TAKE_VALUE_THREE")	
		$arrTakeThree[] = $alfavit[$litera];		
	
	}
	
	foreach ($arrModifySubaction as $litera=>$action)
	{
	if ($action=="XLS_DEL_SUBSTR")
		$arrDelSubstr[] = $litera;
	}

$tmpColumns = $_REQUEST['columns'];
if ((count($tmpColumns[$_REQUEST['fld_identify']])==0)&&($_REQUEST['fld_identify']!==$_REQUEST['fld_name']))
	{
ShowError(GetMessage("WRONG_IDENTIFY_CODE"));
die();
	}
	
	//print "<pre>"; print_r($arrTakeTwo); print "</pre>";
	
	$_SESSION["MCART_XLS_ARRAY"] = 
	array
	(
	'COLUMNS'=>$_REQUEST['columns'],
	'ACTION_MODIFY'=>array('TO_INT'=>$arrToInt, 'TO_LINK'=>$arrToLink, 'TAKE_TWO'=>$arrTakeTwo, "TAKE_THREE"=>$arrTakeThree, "SUBACTION_PARAMS"=>$arrActionParams, "DEL_SUBSTR"=>$arrDelSubstr),
	'NAME_ID'=>$_REQUEST['fld_name'],
	'IDENTIFY'=>$_REQUEST['fld_identify'],
	'INPUT_FILENAME'=>$_REQUEST['xls_input_filename'],
	'IBLOCK_ID'=>$_REQUEST['xls_iblock_id'],
	'SECTION'=>$_REQUEST['xls_iblock_section_id'],
	'SECTION_FOR_NEW'=>$_REQUEST['xls_iblock_section_id_new'],
	'CATALOG_PRICE_BASE_ID'=>$_REQUEST['catalog_base_price_id'],
	'SKU_IBLOCK_ID'=>(intval($_REQUEST['sku_iblock_id'])>0 ? $_REQUEST['sku_iblock_id'] : 0),
	'CML2_LINK_CODE'=>(intval($CML2_LINK_CODE)>0 ? $CML2_LINK_CODE : 0),
	"FIRST_ROW"=>$firstRow,
	"HIGHEST_ROW"=>IntVal($_REQUEST["xls_highest_row"]),
	"DIAPAZONE_A"=>$firstColumn,
	"DIAPAZONE_Z"=>$highestColumn,
	"SHEET_ID" =>$_SESSION['ARR_XLS_DATA']["SHEET_ID"],
	"LAST_ROW_TYPE"=>$_SESSION['ARR_XLS_DATA']["LAST_ROW_TYPE"],
	'ERR_COUNT'=>0,
	'UPDATE_COUNT'=>0, 
	'ADD_COUNT'=>0
	);
	
	if ($IS_SAVE_PROFILE):
	$profile = new CMcartXlsProfile();
	
	$arrData = array(
	"NAME"=>$PROFILE_NAME,
	"ACTIONS"=>$arrModify,
	"IBLOCK_ID"=>$_REQUEST['xls_iblock_id'],
	"SECTION_ID"=>$_REQUEST['xls_iblock_section_id'],
	"NAME_FIELD"=>$_REQUEST['fld_name'],
	"IDENTIFY" => $_REQUEST['fld_identify'],
	"DATA_ROW"=>$firstRow,
	"TITLE_ROW"=>$titleRow,
	"DIAPAZONE_A"=>$firstColumn,
	"DIAPAZONE_Z"=>$highestColumn,
	"FIELDS"=>$_REQUEST['columns'],
	"SHEET_ID" =>$_SESSION['ARR_XLS_DATA']["SHEET_ID"],
	"LAST_ROW_TYPE"=>$_SESSION['ARR_XLS_DATA']["LAST_ROW_TYPE"],
	"SKU_IBLOCK_ID" => (intval($_REQUEST['sku_iblock_id'])>0 ? $_REQUEST['sku_iblock_id'] : 0),
	"CML2_LINK_CODE" =>(intval($CML2_LINK_CODE)>0 ? $CML2_LINK_CODE : 0),
	"SECTION_NEW" =>$_REQUEST['xls_iblock_section_id_new'],
	"NEED_TRANSLIT"=>$MAKE_TRANSLIT_CODE
	);
	
	$profile->Add($arrData);
	
endif;

?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mcart.xls/admin/mcart_xls_import_step_3.php");?>
<?


/*	



*/	
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>