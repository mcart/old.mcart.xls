CREATE TABLE IF NOT EXISTS main_profile (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(200) NOT NULL,
  worksheet int(10) NOT NULL,
  iblock_id int(10) NOT NULL,
  row_first int(10) NOT NULL,
  row_title int(10) NOT NULL,
  column_firsl varchar(2) NOT NULL,
  column_last varchar(2) NOT NULL,
  row_end_label varchar(15) NOT NULL,
  need_offer tinyint(1) NOT NULL,
  section_id int(10) NOT NULL,
  section_id_new int(10) NOT NULL,
  need_translit tinyint(1) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
