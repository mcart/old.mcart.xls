CREATE TABLE IF NOT EXISTS mcart_profile_property (
  id int(11) NOT NULL AUTO_INCREMENT,
  profile_id int(10) NOT NULL,
  column_litera varchar(4) NOT NULL,
  field_code varchar(200) NOT NULL,
  action varchar(200) NOT NULL,
  subaction varchar(20) NOT NULL,
  params text NOT NULL,
  identify tinyint(1) NOT NULL,
  PRIMARY KEY (id)
)ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;